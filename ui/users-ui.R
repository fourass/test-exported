# Second tab content
tabItem(tabName = "Utilisateurs",
        setShadow("box"),
        setZoom("box", scale = 1.005),
        setPulse("box",duration = 0.5,iteration = 5),
        fluidPage(
          
          # begin: datepicker
          fluidRow(
            box(
              width = 5,
              background = "navy",
              collapsible = TRUE, title = "Periode",
              dateRangeInput("datepickerUser","Start date and finish date :", NULL,width = "98%", start = Sys.Date() - 7)
            ),
            infoBoxOutput("diffUsers", width = 5)
          ),
          #End : datepicker

          fluidRow(
            column(
              width = 12,
              fluidRow(
                box(
                  title = "User by date",
                  background = "navy",
                  collapsible = TRUE,
                  width = 12,
                  align="center",
                  withLoader(htmlOutput("trend_plot"),type="html"))
              )),
            column(
              width = 6,
              fluidRow(
                box(
                  title = "Users by age",
                  background = "navy",
                  collapsible = TRUE,
                  width = 12,
                  height = 460,
                   withLoader(htmlOutput("usersAge"),type="html")
                ))),
            column(
              width = 6,
              fluidRow(
                box(
                  title = "Most  visited browsers",
                  background = "navy",
                  collapsible = TRUE,
                  width = 12,
                  height = 460,
                  withLoader(htmlOutput("browser"),type="html"))
              )),
            column(
              width = 12,
              fluidRow(
                box(
                  title = "Most  used devices",
                  background = "navy",
                  collapsible = TRUE,
                  width = 12,
                  height = 460,
                  withLoader(htmlOutput("usersMobile"),type="html"))
              )),

              fluidRow(
                column(
                  width=12,
                  box(
                    title = "Users by country",
                    background = "navy",
                    collapsible = TRUE,
                    width=12,
                    height = 460,
                    align="center",
                    withLoader(htmlOutput("gvis"),type="html")
                  )))

            )
            #end of fluidRow

            )#end of column
        )

#end of fluiPage
