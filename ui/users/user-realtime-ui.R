  tabItem(tabName = "reelTimeUser",
        fluidPage(
          column(
            width=12,
          fluidRow(
            valueBox(width=12,subtitle="Utilisateurs actifs actuellement",value=textOutput("usersActive"),icon=shiny::icon("users"),color='navy')
            
          )),
          
          fluidRow(
            column(
              width= 6,
            box(
              title = "Most  used devices",
              background = "navy",
              collapsible = TRUE,
              width = 12,
              height = 460,
              withLoader(htmlOutput("usersMobileReal"),type="html")
          )),
          column(
            width=6,
          box(
            title = "Most  used devices",
            background = "navy",
            collapsible = TRUE,
            width = 12,
            height = 460,
            withLoader(htmlOutput("usersCountryReal"),type="html")
          ))
          ),
          fluidRow(
            column(
              width = 12,
              box(
                title ="nombre d utilisateurs par page",
                collapsible = TRUE,
                width = 12,
                withLoader(htmlOutput("userPageReal"),type = "html")))


          )
        )
  )